package RedditAnalyzer;

import net.dean.jraw.RedditClient;
import net.dean.jraw.http.NetworkAdapter;
import net.dean.jraw.http.OkHttpNetworkAdapter;
import net.dean.jraw.http.UserAgent;
import net.dean.jraw.models.PublicContribution;
import net.dean.jraw.models.Submission;
import net.dean.jraw.models.TimePeriod;
import net.dean.jraw.oauth.Credentials;
import net.dean.jraw.oauth.OAuthHelper;
import net.dean.jraw.pagination.DefaultPaginator;
import net.dean.jraw.references.SubredditReference;
import net.dean.jraw.tree.CommentNode;
import net.dean.jraw.tree.RootCommentNode;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.System.err;
import static java.lang.System.out;
import static java.util.List.of;

public class App {

    private static final String TITLE_SPLIT_REGEX = "\\s+";
    private static final String BODY_SPLIT_REGEX = "\\s+";
    private static final String COMMENT_SPLIT_REGEX = "\\s+";

    private static final String TITLE_CLEANUP_REGEX = "[?!,.&()\";\\]\\[*#]";
    private static final String BODY_CLEANUP_REGEX = "[?!,.&()\";\\]\\[*#]";
    private static final String COMMENT_CLEANUP_REGEX = "[?!,.&()\";\\]\\[*#]";




    public static void main(String[] args) {

        if(args.length != 3){
            System.err.println("Argument mismatch found " + args.length + " instead of 3");
            return;
        }

        String username = args[0];
        String password = args[1];
        String subreddit = args[2];

        out.println("Logging in: ");
        out.println("\tUser: " + username);
        out.println("\tPassword: " + "*".repeat(password.length()));
        out.println("\tSubreddit: " + subreddit);

        RedditClient reddit = authenticate(username, password);


        SubredditReference reference = gatherSubredditInfo(reddit, subreddit);
        analyzeSubreddit(reddit, reference, 100, TimePeriod.YEAR);


        System.exit(0);
    }

    private static RedditClient authenticate(String username, String password){

        UserAgent userAgent = new UserAgent("bot", "com.AstralJaeger.RedditAnalyzer", "v1.0", username);
        Credentials credentials = Credentials.script(username, password, Constants.CLIENT_ID, Constants.API_SECRET);
        NetworkAdapter adapter = new OkHttpNetworkAdapter(userAgent);
        return OAuthHelper.automatic(adapter, credentials);
    }

    private static SubredditReference gatherSubredditInfo(RedditClient client, String subreddit){

        out.println("Gathering subreddit information:");
        SubredditReference subredditReference = client.subreddit(subreddit);
        out.println("\t" + subredditReference.about().getName() + ":");
        out.println("\t" + subredditReference.about().getPublicDescription());
        return subredditReference;
    }

    private static void analyzeSubreddit(RedditClient client, SubredditReference reference, int n, TimePeriod period){

        out.println("Gathering last " + n + " submissions form the last " + period.name());
        DefaultPaginator<Submission> paginator = client
                .subreddit(reference.about().getName())
                .posts()
                .timePeriod(period)
                .limit(n)
                .build();

        List<Submission> submissions = paginator.next();
        if(submissions.size() < 20){
            System.err.println("Warning: low sample set!");
        }

        HashMap<String, Integer> wordCount = new HashMap<>();
        HashMap<Character, Integer> letterCount = new HashMap<>();

        // analyze submission header
        submissions.forEach(submission->{
            of(submission
                    .getTitle()
                    .replaceAll(TITLE_CLEANUP_REGEX, "")
                    .split(TITLE_SPLIT_REGEX))
                    .forEach(word->{
                        count(word, wordCount, letterCount);
                    });

            // analyze submission body
            if(submission.isSelfPost()){
                try {
                    of(submission
                            .getSelfText()
                            .replaceAll(BODY_CLEANUP_REGEX, "")
                            .split(BODY_SPLIT_REGEX))
                            .parallelStream()
                            .forEach(word -> count(word, wordCount, letterCount));
                }catch (NullPointerException ignored){
                    // a submission doesnt need to have text can also be only a link
                    out.printf("NullPointerException @ submission %s from %s being a %s %n",
                            submission.getUniqueId(),
                            submission.getUrl(),
                            (submission.isSelfPost() ? "SELF_POST" : "NOT_A_SELF_POST"));
                }
            }

            // analyze submission comments
            // handling comments is very different from handling bodies and titles.
            countCommentContent(client, submission, wordCount, letterCount);

        });


        printWordCount(wordCount);
        printLetterCount(letterCount);

    }

    private static void count(String word, HashMap<String, Integer> wordCount, HashMap<Character, Integer> letterCount){

        word = word.toLowerCase();

        if(wordCount.containsKey(word)){
            wordCount.put(word, wordCount.get(word) + 1);
        }else {
            wordCount.put(word, 1);
        }

        word.chars()
                .forEach(c->{
                    if(letterCount.containsKey((char) c)){
                        letterCount.put((char) c, letterCount.get((char) c) + 1);
                    } else {
                        letterCount.put((char) c, 1);
                    }
                });
    }

    private static void printWordCount(HashMap<String, Integer> wordCount){

        out.println("===== Word count report: ");
        int total = wordCount
                .values()
                .stream()
                .mapToInt(x-> x)
                .sum();

        out.println("Total counted: " + total);

        Map<Integer, List<String>> valueMap = wordCount
                .keySet()
                .parallelStream()
                .collect(Collectors.groupingBy(wordCount::get));

        final int max_printed = 25;

        valueMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.<Integer, List<String>>comparingByKey().reversed())
                .forEach(set -> out.printf("\t %5d (%7.4f%c): %s%n",
                        set.getKey(),
                        (set.getKey()/(float)total)*100,
                        '%',
                        set
                                .getValue()
                                .parallelStream()
                                .limit(max_printed)
                                .collect(Collectors.joining(", ")) +
                                (set.getValue().size() > max_printed ?
                                        String.format(" +%d others", set.getValue().size() - max_printed): "")));
    }

    private static void printLetterCount(HashMap<Character, Integer> letterCount){

        out.println("===== Letter count report:");
        int total = letterCount
                .values()
                .parallelStream()
                .mapToInt(x->x)
                .sum();
        out.println("Total counted: " + total);

        Map<Integer, List<Character>> valueMap = letterCount
                .keySet()
                .parallelStream()
                .collect(Collectors.groupingBy(letterCount::get));

        valueMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.<Integer, List<Character>>comparingByKey().reversed())
                .forEach(set -> out.printf("\t %5d (%5.2f%c): %s%n", set.getKey(),(set.getKey()/(float)total)*100, '%', set
                        .getValue()
                .stream()
                .map(c->"" + c)
                .collect(Collectors.joining(", "))));
    }

    private static void countCommentContent(RedditClient client, Submission submission, HashMap<String, Integer> wordCount, HashMap<Character, Integer> letterCount){

        RootCommentNode root = client.submission(submission.getId()).comments();
        Iterator<CommentNode<PublicContribution<?>>> iterator = root.walkTree().iterator();
        while (iterator.hasNext()){
            PublicContribution<?> thing = iterator.next().getSubject();
            try {
                String body = thing.getBody();
                of(body
                        .replaceAll(BODY_CLEANUP_REGEX, "")
                        .split(BODY_SPLIT_REGEX))
                        .parallelStream()
                        .forEach(word -> count(word, wordCount, letterCount));
            }catch (NullPointerException ignored){
            }

        }
    }

}
